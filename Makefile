#!make

# List of local docker-dev images
DD_IMAGES = $(shell find ../../docker-dev/ -maxdepth 1 -type d -not -name 'common' -and -not -name 'docker-dev')

all: info

.PHONY: info
info:
	$(info Geekstuff.dev / WSL2 / Common / Devcontainer)
	$(info > make builds)
	$(info > make packages)
	@:

.PHONY: .all
.all:
	@$(foreach DD_IMAGE,$(DD_IMAGES),$(MAKE) -C $(DD_IMAGE) ${ACTION};)

.PHONY: builds
builds: ACTION=build
builds: .all

.PHONY: packages
packages: ACTION=package
packages: .all

.PHONY: print
print:
	@$(foreach DD_IMAGE,$(DD_IMAGES),echo $(DD_IMAGE);)
