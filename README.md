# WSL2 / Common / Devcontainer

This project and the `dev.code-workspace` holds in it a tiny a multi-root
Alpine devcontainer meant to iterate on current `WSL2 / **` projects.

More specifically this is the devcontainer for projects in `WSL2 / Common / *`
and `WSL2 / Docker-Dev / *` at the moment.

- [WSL2 / Common / Devcontainer](#wsl2--common--devcontainer)
  - [How to setup](#how-to-setup)
  - [How to start](#how-to-start)

## How to setup

In short, locally git clone a replica of [wsl2/common](https://gitlab.com/geekstuff.dev/wsl2/common)
and [wsl2/docker-dev](https://gitlab.com/geekstuff.dev/wsl2/docker-dev) projects,
in a linux environment which has access to a docker engine and can mount local folders.

If using WSL2 and you don't have the above or you are having issues running devcontainers,
you should consider consuming one of the docker-dev image which will provide the requirements
in no time to run devcontainers.

Here are the steps to do, mostly in a linux terminal:

1. Somewhere on your linux filesystem, create a new local folder `wsl2` and subfolders
  `common` and `docker-dev`.

```
mkdir -p wsl2/common wsl2/docker-dev
```

2. Clone both [wsl2/common](https://gitlab.com/geekstuff.dev/wsl2/common) projects
   inside the matching local subfolder.

```
git clone https://gitlab.com/geekstuff.dev/wsl2/common/dev-setup.git wsl2/common/dev-setup
git clone https://gitlab.com/geekstuff.dev/wsl2/common/devcontainer.git wsl2/common/devcontainer
```

3. Clone one or all of [wsl2/docker-dev](https://gitlab.com/geekstuff.dev/wsl2/docker-dev) projects
  inside the matching local subfolder.

```
git clone https://gitlab.com/geekstuff.dev/wsl2/docker-dev/ubuntu-22.04-systemd wsl2/docker-dev/ubuntu-22.04-systemd
git clone https://gitlab.com/geekstuff.dev/wsl2/docker-dev/ubuntu-22.04 wsl2/docker-dev/ubuntu-22.04
git clone https://gitlab.com/geekstuff.dev/wsl2/docker-dev/alpine wsl2/docker-dev/alpine
```

3. In VSCode, ensure the [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
    extension pack is installed, then quit VSCode.

## How to start

This project is a multi-root devcontainer. Here is how to start it.

1. Open workspace in VSCode, from WSL terminal:

```
code -n wsl2/common/devcontainer/dev.code-workspace
```

2. Reopen project in devcontainer.
    - A prompt may appear automatically to do so, if prompts are not disabled.
    - Otherwise press F1 and search `Reopen in devcontainer`

Once the devcontainer is started, you should see a few linux terminals automatically
appear inside VSCode, for each git projects, with example commands to do a few things
in them such as building and packaging a WSL distro, running an ephemeral shell in them
to test things, etc.
